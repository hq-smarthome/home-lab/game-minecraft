job "game-minecraft" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  vault {
    policies = ["job-game-minecraft"]
  }

  group "minecraft" {
    count = 1

    network {
      port "minecraft" {
        static = 25565
      }

      port "rcon" {}
    }

    volume "minecraft" {
      type = "host"
      read_only = false
      source = "minecraft"
    }

    task "minecraft" {
      driver = "docker"

      kill_timeout = "90s"
      kill_signal = "SIGTERM"

      volume_mount {
        volume = "minecraft"
        destination = "/data"
        read_only = false
      }

      config {
        image = "itzg/minecraft-server:latest"

        ports = ["minecraft"]
      }

      template {
        data = <<EOH
[[ fileContents "./config/minecraft.template.env" ]]
        EOH

        destination = "secrets/minecraft.env"
        env = true
      }

      resources {
        cpu = 4000
        memory = 8192
      }

      service {
        name = "minecraft"
        port = "minecraft"

        check {
          name = "Minecraft"
          port = "minecraft"
          type = "tcp"
          interval = "30s"
          timeout = "10s"
          task = "minecraft"
        }
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
